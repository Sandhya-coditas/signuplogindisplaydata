import { Injectable } from '@angular/core';
import {HttpClient,HttpHeaders} from '@angular/common/http';
import { IData } from './types';
// import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  headers = new HttpHeaders().set("ngrok-skip-browser-warning", "1234");
  // {'headers':this.headers}

  constructor(private http:HttpClient) { }

  postSignUp(user:any){
    console.log(user)
    return this.http.post("https://80ee-103-36-44-106.in.ngrok.io/register",user,{'headers':this.headers});
  }
  postLogin(person:any){
    return this.http.post("https://80ee-103-36-44-106.in.ngrok.io/login",person,{'headers':this.headers});
  }
  getUserData(data1:any){
   
    return this.http.get<IData[]>(`https://80ee-103-36-44-106.in.ngrok.io/user/getUsers/${data1.jwtId}`,
    {headers:{
      "Authorization":"Bearer"+ data1.jwtToken,
      "ngrok-skip-browser-warning":"1234",
     'Content-Type':'application/json}',
      },
    },);


  }
}























//   signUp(userName:string,userEmail:string,password:string,userCity:string,userRole:string,userSalary:number):Observable<any>{
//     return this.http.post(authApi+'signUp',{
//       userName,
//       userEmail,
//       password,
//       userCity,
//       userRole,
//       userSalary
//     },httpOptions);
//   }
  
  
//   login(userEmail:string,password:string):Observable<any>{
//     return this.http.post(authApi+'login',{
//       userEmail,
//       password

//     },httpOptions);
//   }


// }
