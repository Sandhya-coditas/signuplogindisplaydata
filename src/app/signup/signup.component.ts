import { Component} from '@angular/core';
import {AuthenticationService } from '../authentication.service';
import {FormGroup,FormControl} from '@angular/forms';
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent {

  constructor(private authenticateService:AuthenticationService){};

 profileForm=new FormGroup({
 userName:new FormControl(''),
 userEmail:new FormControl(''),
 password:new FormControl(''),
 userCity:new FormControl(''),
 userRole:new FormControl(''),
 userSalary:new FormControl(''),
 });
 data:any={};
 
 onSubmit(){
  console.warn(this.profileForm.value);
  this.authenticateService.postSignUp(this.profileForm.value).subscribe({
    next:(response:any)=>{
      this.data=response;
      console.log(this.data);
 }
  
})
 }
}



 
 
  // form:any={
  //   userName:string='',
  //   userEmail:string:'',
  //   password:null,
  //   userCity:null,
  //   userRole:null,
  //   userSalary:null
  // };
  // constructor(private authService:AuthenticationService) { }
  // onSubmit():void{
  //   const {userName,userEmail,password,userCity,userRole,userSalary}=this.form;

  //   this.authService.signUp(userName,userEmail,password,userCity,userRole,userSalary).subscribe({
  //     next:data=>{
  //       console.log(data);
  //       this.isSuccessful=true;
  //       this.isSignUpFailed=false;
  //     },
  //     error:err=>{
  //       this.errorMessage=err.error.message;
  //       this.isSignUpFailed=true;
  //     }
  //   })
  // }

