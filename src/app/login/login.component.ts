import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent {
  constructor(private authenticateService: AuthenticationService) { };
  @Output() token = new EventEmitter();
  @Output() tokenId = new EventEmitter();
  @Output() displayData = new EventEmitter();

  jwtToken = '';
  jwtId = '';
  id:string=this.jwtId;
  data: any = {};
  data1:any={token1:this.jwtToken,id1:this.jwtId}

  loginForm = new FormGroup({
    userEmail: new FormControl(''),
    password: new FormControl(''),
  });
 
  onLogin() {
    console.warn(this.loginForm.value);
    this.authenticateService.postLogin(this.loginForm.value).subscribe({
      next: (response: any) => {
        this.data = response;
        console.log(this.data);
        this.token.emit(this.jwtToken);
        this.tokenId.emit(this.jwtId);
        this.displayData.emit(this.data1);
        // console.log(this.data1);

       
      },
      complete: ()=>{
        this.getData(this.data1)
      },
    })
  }
  getData(data1:{}){
    this.authenticateService.getUserData(data1).subscribe((result)=>{
      console.log(result)
    });
  }
}



















