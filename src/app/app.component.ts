import { Component ,Input } from '@angular/core';
import {AuthenticationService } from './authentication.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private authenticateService:AuthenticationService){};
  title = 'loginAndSignUp';
  displayData:any={};
 
  data11 !: any[]
  getData(data1:{}){
    this.authenticateService.getUserData(data1).subscribe((result)=>{
      console.log(result)
      data1 = this.data11
    });
  }

}
